const neo4j = require('neo4j-driver')
const config = require('dotenv').config()

class Neo4jConnection {
    constructor() {
        this.db = new neo4j.driver(
            config.parsed.NEO4J_HOST,
            neo4j.auth.basic(config.parsed.NEO4J_USER,config.parsed.NEO4J_PASSWORD)
        )
    }

    async read(query,params) {
        let session = this.db.session()
        let data = [];
        await session.run(query,params).then(
            results => {
                results.records.forEach(result => {
                    data.push(result._fields[0].properties);
                })
            }
        )
        .catch(err => {
            console.log(err)
        })
        return data;
    }
}

module.exports = new Neo4jConnection()