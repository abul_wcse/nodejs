const { toGraphQLTypeDefs } = require("@neo4j/introspector")
const neo4j = require("neo4j-driver");
const fs = require('fs');
const config = require('dotenv').config({
    path: './../../.env'
})

const driver = neo4j.driver(
    config.parsed.NEO4J_HOST,
    neo4j.auth.basic(config.parsed.NEO4J_USER,config.parsed.NEO4J_PASSWORD)
);

const sessionFactory = () => driver.session({ defaultAccessMode: neo4j.session.READ })

// We create a async function here until "top level await" has landed
// so we can use async/await
async function main() {
    const typeDefs = await toGraphQLTypeDefs(sessionFactory)
    fs.writeFileSync('./../../graphql/schema.graphql', typeDefs)
    await driver.close();
}
main()