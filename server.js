const express = require("express");
const authorRouter = require('./routes/author')
const db = require('./lib/db');
const config = require('dotenv').config()

app = express()
app.listen(config.parsed.PORT, () => { console.log("server is running in pert " + config.parsed.PORT )});
app.use('/author', authorRouter)
