const express = require('express')
const router = express.Router()
const { graphqlHTTP } = require('express-graphql');
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLNullableType
} = require('graphql');

const db = require('../lib/db')

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    description: 'Contains the Author details',
    fields: () => ({
        id: { type: GraphQLInt },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        dateOfBirth: { type: GraphQLString },
        books: {
            type: new GraphQLList(BookType),
            resolve: async (author) => await db.read("MATCH(b:Book) WHERE b.authorId = " + author.id + " RETURN(b)")
        }
    })
})

const BookType = new GraphQLObjectType({
    name: 'Book',
    description: 'Book detail',
    fields: () => ({
        id: { type: GraphQLInt },
        name: { type: GraphQLString },
        isbn: { type: GraphQLString },
        language: { type: GraphQLString },
        authorId: { type: GraphQLInt }
    })
})

const RootQueryType = new GraphQLObjectType({
    name: 'Query',
    description: 'Root Query type',
    fields: () => ({
        author: {
            type: new GraphQLList(AuthorType),
            description: 'List of all author',
            resolve: async () => await db.read('MATCH(n:Author) RETURN(n)')
        }
    })
})

const schema = new GraphQLSchema({
    query: RootQueryType
})

router.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
}))

module.exports = router